package com.haseebazeem.dreamzest.Model;

public class Book {

    private String name;
    private String fileUrl;

    public Book(String name, String fileUrl) {
        this.name = name;
        this.fileUrl = fileUrl;
    }

    public String getName() {
        return name;
    }

    public String getFileUrl() {
        return fileUrl;
    }
}
