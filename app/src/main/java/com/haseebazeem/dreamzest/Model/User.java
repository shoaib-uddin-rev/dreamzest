package com.haseebazeem.dreamzest.Model;

import java.util.Map;

public class User {

    private String name;
    private String username;
    private String email;
    private String avatar = "";
    private int isApproved = 0;
    private String phoneNumber;
    private String countryCode = "";
    private String userID;
    private String endSubsDate;
    private String stripeSubscriptionId;
    private String payPalSubscriptionId;

    public User(User user) {
        name = user.name;
        username = user.username;
        email = user.email;
        avatar = user.avatar;
        userID = user.userID;
        endSubsDate = user.endSubsDate;
        countryCode = user.countryCode;
        phoneNumber = user.phoneNumber;
        isApproved = user.isApproved;
        stripeSubscriptionId = user.stripeSubscriptionId;
        payPalSubscriptionId = user.payPalSubscriptionId;
    }

    public User() {
    }

    public User(Map<String, Object> map) {
        name = String.valueOf(map.get("name"));
        username = String.valueOf(map.get("username"));
        email = String.valueOf(map.get("email"));
        avatar = String.valueOf(map.get("picUrl"));
        userID = String.valueOf(map.get("userID"));
        countryCode = String.valueOf(map.get("countryCode"));
        phoneNumber = String.valueOf(map.get("phoneNumber"));
        endSubsDate = String.valueOf(map.get("endSubsDate"));
//        this.isApproved = (Integer) map.get("isApproved");
        stripeSubscriptionId = String.valueOf(map.get("stripe_subscription_id"));
        payPalSubscriptionId = String.valueOf(map.get("paypal_subscription_id"));
    }

    public String getPayPalSubscriptionId() {
        return payPalSubscriptionId;
    }

    public void setPayPalSubscriptionId(String payPalSubscriptionId) {
        this.payPalSubscriptionId = payPalSubscriptionId;
    }

    public String getStripeSubscriptionId() {
        return stripeSubscriptionId;
    }

    public void setStripeSubscriptionId(String stripeSubscriptionId) {
        this.stripeSubscriptionId = stripeSubscriptionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(int isApproved) {
        this.isApproved = isApproved;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEndSubsDate() {
        return endSubsDate;
    }

    public void setEndSubsDate(String endSubsDate) {
        this.endSubsDate = endSubsDate;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", avatar='" + avatar + '\'' +
                ", isApproved=" + isApproved +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", countryCode='" + countryCode + '\'' +
                ", userID='" + userID + '\'' +
                ", endSubsDate='" + endSubsDate + '\'' +
                ", stripeSubscriptionId='" + stripeSubscriptionId + '\'' +
                ", payPalSubscriptionId='" + payPalSubscriptionId + '\'' +
                '}';
    }
}
