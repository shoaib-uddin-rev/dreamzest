package com.haseebazeem.dreamzest.data;


import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.Subscription;
import com.haseebazeem.dreamzest.data.model.keywords.Interpretation;
import com.haseebazeem.dreamzest.data.model.keywords.KeywordList;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.haseebazeem.dreamzest.Utils.Constants.API_CANCEL_SUBSCRIPTION;
import static com.haseebazeem.dreamzest.Utils.Constants.API_INTERPRETATION;
import static com.haseebazeem.dreamzest.Utils.Constants.API_KEYWORD;
import static com.haseebazeem.dreamzest.Utils.Constants.API_KEYWORD_SEARCH;
import static com.haseebazeem.dreamzest.Utils.Constants.API_SUBSCRIBE;


public interface WebServices {


    @GET(API_KEYWORD)
    Single<BaseResponse<KeywordList>> getKeywords(@Query("subscription_id") String subscriptionId, @Query("page") int page);

    @GET(API_KEYWORD_SEARCH)
    Observable<BaseResponse<KeywordList>> searchKeywords(@Query("subscription_id") String subscriptionId, @Query("search") String query,

                                                         @Query("page") int page);

    @GET(API_INTERPRETATION)
    Single<BaseResponse<Interpretation>> getInterpretation(@Path("id") int id, @Query("subscription_id") String subscriptionId);

    @FormUrlEncoded
    @POST(API_SUBSCRIBE)
    Single<BaseResponse<Subscription>> subscribe(@Field("card_no") String cardNo, @Field("exp_mon") String month, @Field("exp_year") String year, @Field("cvv") String cvv,
                                                 @Field("plan") String planId, @Field("subs_user_id") String subs_user_id, @Field("email") String email
    );

    @FormUrlEncoded
    @POST(API_CANCEL_SUBSCRIPTION)
    Single<BaseResponse<String>> cancelSubscription(@Field("subscription_id") String subscriptionId, @Field("subscription_type") String type);

//    @FormUrlEncoded
//    @POST(API_LOGIN)
//    Single<BaseResponse<UserDetail>> callLoginAPI(
//            @FieldMap Map<String, String> params
//    );

//    @FormUrlEncoded
//    @POST(API_SOCIAL_LOGIN)
//    Single<BaseResponse<UserDetail>> callSocailLoginAPI(
//            @FieldMap Map<String, String> params
//    );
//
//
//    @FormUrlEncoded
//    @POST(API_FORGET_PASSWORD)
//    Single<BaseResponse<ForgetPassword>> callForgetPasswordAPI(
//            @FieldMap Map<String, String> params
//    );
//
//
//    @FormUrlEncoded
//    @POST(API_FORGET_PASSWORD_VERIFIED_OTP)
//    Single<BaseResponse<ForgetPassword>> callVerifiedOtpAPI(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST(API_SESSION_TOKEN)
//    Single<BaseResponse<SessionResponse>> getSessionToken(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_FORGET_PASSWORD_NEW_PASSWORD)
//    Single<BaseResponse<ForgetPassword>> callNewPasswordAPI(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST(API_PRODUCT_DETAIL)
//    Single<BaseResponse<ProductDetail>> getProductDetail(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST(API_CATEGORY)
//    Single<BaseResponse<CategoryResponse>> getCategory(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST(API_BRAND)
//    Single<BaseResponse<BrandResponse>> getBrand(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST(API_SELLER)
//    Single<BaseResponse<SellerResponse>> getSeller(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST(API_SIGN_UP_TOKEN)
//    Single<BaseResponse<SignUpOtp>> getSignUpOtp(
//            @FieldMap Map<String, String> params
//    );
//
//    @FormUrlEncoded
//    @POST(API_SIGN_UP)
//    Single<BaseResponse<SignUpOtp>> getSignUp(
//            @FieldMap Map<String, String> params
//    );
//
//
//    @FormUrlEncoded
//    @POST(API_FILTER)
//    Single<BaseResponse<FilterResponse>> getFilters(@Path("page_type") String pageType,
//                                                    @FieldMap Map<String, String> params
//    );
//
//
//    @FormUrlEncoded
//    @POST(API_SESSION_TOKEN)
//    Single<BaseResponse<SessionResponse>> getToken(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CATEGORY_MENU)
//    Single<BaseResponse<MenuResponse>> getCategoryMenu(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_ADD_TO_CART)
//    Single<BaseResponse<CartCount>> addToCart(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_DESTROY_CART)
//    Single<BaseResponse<Void>> destroyCart(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_ADD_TO_WISH_LIST)
//    Single<BaseResponse<BaseMessage>> addToWishList(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_MY_WISH_LIST)
//    Single<BaseResponse<WishlistResponse>> getMyWishList(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_REMOVE_MY_WISH_LIST)
//    Single<BaseResponse<BaseMessage>> removeWishListSingleItem(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_MY_CART_LIST)
//    Single<BaseResponse<MyCartResponse>> getMyCartList(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_REMOVE_MY_CART_ITEM)
//    Single<BaseResponse<BaseMessage>> removeCartItem(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_UPDATE_MY_CART)
//    Single<BaseResponse<BaseMessage>> updateCart(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_RE_LOGIN)
//    Single<BaseResponse<UserDetail>> relogin(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_HOME_PAGE_DATA)
//    Single<BaseResponse<HomeResponse>> getHomePageList(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CITY_LIST)
//    Single<BaseResponse<CityResponse>> getAllCities(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_DELIVERY_COST)
//    Single<BaseResponse<DeliveryCostResponse>> getDeliveryCostByCity(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_ADD_ADDRESS)
//    Single<BaseResponse<Void>> addCustomerAddress(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_AREA_CITY)
//    Single<BaseResponse<AreaResponse>> getAllAreasByCity(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_ADDRESS_LIST)
//    Single<BaseResponse<AddressResponse>> getAllCustomerAddress(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_UPDATE_ADDRESS)
//    Single<BaseResponse<Void>> updateAddress(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_ADDRESS_BY_ID)
//    Single<BaseResponse<AddressByIdResponse>> getAddressById(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_ORDER_SUMMARY)
//    Single<BaseResponse<OrderSummaryResponse>> getMyOrderSummary(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_APPLY_COUPON)
//    Single<BaseResponse<ApplyCouponResponse>> getApplyCoupon(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_APPLY_COUPON)
//    Single<BaseResponse<BaseMessage>> removeCoupon(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_DEFAULT_ADDRESS)
//    Single<BaseResponse<DefaultAddressResponse>> getDefaultAddress(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_NOTIFICATION)
//    Single<BaseResponse<NotificationsResponse>> getNotificationList(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CREATE_ORDER)
//    Single<BaseResponse<NewOrderResponse>> createOrder(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_SEARCH)
//    Observable<BaseResponse<SearchResponse>> getSearchResult(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CUSTOMER_ACCOUNT)
//    Single<BaseResponse<AccountResponse>> getCustomerAccount(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CUSTOMER_BALANCE)
//    Single<BaseResponse<CustomerBalanceResponse>> getCustomerBalance(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CUSTOMER_STATEMENT)
//    Single<BaseResponse<TransactionHistoryResponse>> getCustomerStatement(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_UPDATE_FCM)
//    Single<BaseResponse<Void>> updateFcm(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CHANGE_PASSWORD)
//    Single<BaseResponse<Void>> changePassword(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_PAYMENT_METHOD)
//    Single<BaseResponse<PaymentMethodResponse>> getPaymentMethodList(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_ORDER_HISTORY)
//    Single<BaseResponse<OrderListResponse>> getOrderHistory(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_PAY_NOW)
//    Single<BaseResponse<Void>> payNow(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_APP_UPDATE)
//    Single<BaseResponse<Void>> checkBuild(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_CART_COUNTER)
//    Single<BaseResponse<CartCount>> getCounterCart(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_URL_PARSE)
//    Single<BaseResponse<UrlParseResponse>> parseUrl(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_LOG_OUT)
//    Single<BaseResponse<Void>> logout(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_SEARCH_CATALOG)
//    Single<BaseResponse<CatalogSearchResponse>> getSearchCatalog(@FieldMap Map<String, String> params);
//
//
//    @FormUrlEncoded
//    @POST(API_SEARCH_CATALOG_FILTER)
//    Single<BaseResponse<FilterResponse>> getSearchCatalogFILTER(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_DEBIT_CARD_UPDATE)
//    Single<BaseResponse<Void>> updateDebitCard(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_ADD_ADDRESS_CHANGE)
//    Single<BaseResponse<Void>> AddAddressCardInfo(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_CUSTOMER_FUNDS_TRANSFER)
//    Single<BaseResponse<FundsTransferResponse>> fundsTransfer(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_ALFA_WALLET_OTP)
//    Single<BaseResponse<NewOrderResponse>> verifyBankAlfalahOtp(@FieldMap Map<String, String> params);
//
//    @FormUrlEncoded
//    @POST(API_PRODUCT_REPORT_API)
//    Single<BaseResponse<Void>> reportProduct(@FieldMap Map<String, String> params);

}
