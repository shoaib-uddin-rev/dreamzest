package com.haseebazeem.dreamzest.data;

import com.haseebazeem.dreamzest.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.haseebazeem.dreamzest.Utils.Constants.BASE_URL;

public class WebFactory {

    private static Retrofit retrofit;
    private static WebServices webServices;





    private static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .readTimeout ( 60, TimeUnit.SECONDS )
                    .connectTimeout(60, TimeUnit.SECONDS);
                    if (BuildConfig.DEBUG){
                          builder.addInterceptor(interceptor);
                    }


            retrofit = new retrofit2.Retrofit.Builder ()
                    .baseUrl ( BASE_URL )
                    .client (builder.build () )
                    .addConverterFactory ( GsonConverterFactory.create () )
                    .addCallAdapterFactory ( RxJava2CallAdapterFactory.create () )
                    .build ();
        }
        return retrofit;


    }

    public static WebServices getWebServices () {

        if (webServices == null)

            return webServices = getRetrofitInstance ().create ( WebServices.class );

        return webServices;
    }


}
