package com.haseebazeem.dreamzest.data;

import com.haseebazeem.dreamzest.Utils.LogUtil;
import com.haseebazeem.dreamzest.data.model.BaseResponse;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NetworkHelper {
     static final String TAG = "NetworkHelper";

    //    Consumer<BaseResponse<T>> onSuccess, Consumer<Throwable> onError,
    public static <T> Disposable makeRequest(Single<BaseResponse<T>> single, NetworkResponseListener<BaseResponse<T>> responseListener) {
        return single
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        tBaseResponse -> {

                            if (tBaseResponse != null) {
                                LogUtil.e(TAG, "response " + tBaseResponse);
                                if (tBaseResponse.getSuccess() != null && tBaseResponse.getSuccess()) {

                                    responseListener.onSuccess(tBaseResponse);

                                }  else
                                    responseListener.onError(tBaseResponse);
                            }
                            responseListener.onResponse(tBaseResponse);

                        }, throwable -> {
                            throwable.printStackTrace();
                            LogUtil.e(TAG, "error " + throwable.getMessage());
                            responseListener.onFailure(throwable);

                            if (throwable instanceof UnknownHostException || throwable instanceof SocketTimeoutException) {
                                responseListener.netWorkException(throwable);
                            } else if (throwable instanceof com.google.gson.JsonParseException) {

                                responseListener.jsonParseException(throwable);
                            } else {
                                responseListener.unKnownException(throwable);
                            }


                        }
                );

    }


 /*   public static void generateSessionToken(SessionNavigator navigator, Map<String, String> sessionParams) {
        //                            Util.isSuccessResponse(response);
//                            LoginPref.setApiToken(mContext, response.getData().getToken());
//                            setApiToken();
        //                            Util.showToastError(mContext, throwable);
        GotoApp.getApi().getSessionToken(sessionParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(

                        navigator::handleSuccessResponse, navigator::handleError);
    }

    public static void tryReLogin(Map<String, String> loginParams, UserDetailNavigator navigator) {
        GotoApp.getApi().relogin(loginParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        navigator::handleSuccessResponse, navigator::handleError);
    }*/

}
