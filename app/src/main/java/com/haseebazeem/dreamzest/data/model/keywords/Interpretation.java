package com.haseebazeem.dreamzest.data.model.keywords;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Interpretation {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("interpretation")
    @Expose
    private String interpretation;
    @SerializedName("biblic_solutions")
    @Expose
    private String biblicSolutions;
    @SerializedName("quranic_solutions")
    @Expose
    private String quranicSolutions;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getInterpretation() {
        return interpretation;
    }

    public void setInterpretation(String interpretation) {
        this.interpretation = interpretation;
    }

    public String getBiblicSolutions() {
        return biblicSolutions;
    }

    public void setBiblicSolutions(String biblicSolutions) {
        this.biblicSolutions = biblicSolutions;
    }

    public String getQuranicSolutions() {
        return quranicSolutions;
    }

    public void setQuranicSolutions(String quranicSolutions) {
        this.quranicSolutions = quranicSolutions;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}