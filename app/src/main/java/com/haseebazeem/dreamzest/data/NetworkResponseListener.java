package com.haseebazeem.dreamzest.data;

 public interface NetworkResponseListener<T>  {


    /*
     * onSuccess will be called when response.getResult==1
     * */
     default void onSuccess(T response) {
    }

    /*
     * onError will be called when response.getResult==0
     * */
    default void onError(T response) {
    }


    /*
     * onResponse will be called when api will be successfully completed
     * same as retrofit2 onResponse
     * */

     default void onResponse(T response) {
    }

    /*
     * onFailure will be called when api will be successfully completed
     *same as retrofit2 onFailure
     * */
    default void onFailure(Throwable error) {
    }

     default void onTokenExpired(T error) {
    }

     default void jsonParseException(Throwable error) {
    }

     default void netWorkException(Throwable error) {
    }

     default void unKnownException(Throwable error) {
    }


}
