package com.haseebazeem.dreamzest.Helper;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.haseebazeem.dreamzest.Utils.Utils;

import java.util.HashMap;
import java.util.Map;

public class Rest {

    private static FirebaseFirestore firestore;
    private static FirebaseAuth firebaseAuth;

    public interface RequestCallBack {
        void onComplete();

        void onError();
    }

    public static void Subscribe(final RequestCallBack requestCallBack) {
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        String date = Utils.addThirtyDays();

        Map map = new HashMap();
        map.put("endSubsDate", date);

        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).update(map).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    requestCallBack.onComplete();
                } else {
                    requestCallBack.onError();
                }
            }
        });
    }

    public static void updateSubscription(String id, String subsId, final RequestCallBack requestCallBack) {
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        String date = Utils.addThirtyDays();

        Map map = new HashMap();
        map.put(id, subsId);

        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).update(map).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    requestCallBack.onComplete();
                } else {
                    requestCallBack.onError();
                }
            }
        });
    }

    public static void cancelSubscription(final RequestCallBack requestCallBack) {
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        String date = Utils.addThirtyDays();

        Map map = new HashMap();
        map.put("stripe_subscription_id", null);
        map.put("paypal_subscription_id", null);

        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).update(map).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    requestCallBack.onComplete();
                } else {
                    requestCallBack.onError();
                }
            }
        });
    }

}
