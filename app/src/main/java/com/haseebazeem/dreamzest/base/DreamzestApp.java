package com.haseebazeem.dreamzest.base;

import android.app.Application;

import com.haseebazeem.dreamzest.data.WebFactory;
import com.haseebazeem.dreamzest.data.WebServices;

public class DreamzestApp extends Application {
    private static WebServices api;
    @Override
    public void onCreate() {
        super.onCreate();
        api = WebFactory.getWebServices();
    }
    public static WebServices getApi() {
        return api;
    }
}
