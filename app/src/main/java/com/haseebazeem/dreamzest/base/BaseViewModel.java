package com.haseebazeem.dreamzest.base;


import androidx.lifecycle.ViewModel;

import com.haseebazeem.dreamzest.Utils.LogUtil;
import com.haseebazeem.dreamzest.data.NetworkHelper;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.WebServices;
import com.haseebazeem.dreamzest.data.model.BaseResponse;

import java.lang.ref.WeakReference;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseViewModel<N> extends ViewModel {


    private static final String TAG = BaseViewModel.class.getSimpleName();
    private CompositeDisposable mCompositeDisposable;
    private WeakReference<N> mNavigator;

    protected BaseViewModel() {


    }

    @Override
    protected void onCleared() {
        LogUtil.e(TAG, "onCleared");
        getCompositeDisposable().clear();
        super.onCleared();
    }

    protected CompositeDisposable getCompositeDisposable() {
        if (mCompositeDisposable == null)
            mCompositeDisposable = new CompositeDisposable();
        return mCompositeDisposable;
    }

    protected WebServices getApi() {
        return DreamzestApp.getApi();
    }

    protected N getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }


    //    Consumer<BaseResponse<T>> onSuccess, Consumer<Throwable> onError,
    protected <T> Disposable makeRequest(Single<BaseResponse<T>> single, NetworkResponseListener<BaseResponse<T>> responseListener) {

        return
                NetworkHelper.makeRequest(single, responseListener);

    }
}
