package com.haseebazeem.dreamzest.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.haseebazeem.dreamzest.Activities.CheckoutActivity;
import com.haseebazeem.dreamzest.Activities.WebViewActivity;
import com.haseebazeem.dreamzest.Model.Book;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Utils;

import java.util.List;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    private Context context;
    private List<Book> booksList;
    private String days;

    public BooksAdapter(Context context, List<Book> booksList, String days) {
        this.context = context;
        this.booksList = booksList;
        this.days = days;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.book_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.bookName.setText(booksList.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Utils.daysLeft(days) > 0) {
                    Intent intent = new Intent(context, WebViewActivity.class);
                    intent.putExtra("url", booksList.get(position).getFileUrl());
                    ((Activity) context).startActivity(intent);
                } else {
                    Toast.makeText(context, "Your Subscription has been ended. Buy Subscription Again!", Toast.LENGTH_LONG).show();
                    ((Activity) context).startActivity(new Intent(context, CheckoutActivity.class));
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return booksList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView bookName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bookName = itemView.findViewById(R.id.book_name);
        }
    }

}
