package com.haseebazeem.dreamzest.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.haseebazeem.dreamzest.Activities.DetailActivity;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.data.model.keywords.Keyword;

import java.util.List;

public class KeywordsAdapter extends RecyclerView.Adapter<KeywordsAdapter.ViewHolder> {

    private Context context;
    private List<Keyword> keywordList;
//    private String days;
private  String subscriptionId;
    public KeywordsAdapter(Context context, List<Keyword> keywordList, String subscriptionId) {
        this.context = context;
        this.keywordList = keywordList;
        this.subscriptionId = subscriptionId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_keyword, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
//        holder.bookName.setText(booksList.get(position).getName());
        holder.keywordName.setText(keywordList.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailActivity.class).putExtra("id", keywordList.get(position).getId())
                        .putExtra("keyword", keywordList.get(position).getName())
                        .putExtra("subscriptionId", subscriptionId)
                );
//                if(Utils.daysLeft(days) > 0) {
//                    Intent intent = new Intent(context, WebViewActivity.class);
//                    intent.putExtra("url", booksList.get(position).getFileUrl());
//                    ((Activity) context).startActivity(intent);
//                } else {
//                    Toast.makeText(context, "Your Subscription has been ended. Buy Subscription Again!", Toast.LENGTH_LONG).show();
//                    ((Activity) context).startActivity(new Intent(context, CheckoutActivity.class));
//                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return keywordList == null ? 0 : keywordList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView keywordName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            keywordName = itemView.findViewById(R.id.tv_keyword);
        }
    }

}
