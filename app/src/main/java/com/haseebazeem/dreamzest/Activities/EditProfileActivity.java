package com.haseebazeem.dreamzest.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.haseebazeem.dreamzest.MainActivity;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Auth;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class EditProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    EditText nameBox, usernamBox, emailBox, phoneBox;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;
    private Auth auth;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        auth = new Auth(this);

        toolbar = findViewById(R.id.reg_tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nameBox = findViewById(R.id.name);
        usernamBox = findViewById(R.id.username);
        emailBox = findViewById(R.id.email);
        phoneBox = findViewById(R.id.phone);

        if (auth.getCurrentUser() != null) {
            fillBoxes();
        } else {
            fetchUser();
        }
    }

    private void fetchUser() {
        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    currentUser = new User(documentSnapshot.getData());
                    fillBoxes();
                } else {
                    currentUser = null;
                    Toast.makeText(EditProfileActivity.this, "This user does not exist any more", Toast.LENGTH_SHORT).show();
                    logout();
                }
            }
        });
    }

    private void fillBoxes() {
        if(auth.getCurrentUser() != null) {
            nameBox.setText(auth.getCurrentUser().getName());
            usernamBox.setText(auth.getCurrentUser().getUsername());
            emailBox.setText(auth.getCurrentUser().getEmail());
            phoneBox.setText(auth.getCurrentUser().getPhoneNumber());
        } else if (currentUser != null) {
            nameBox.setText(currentUser.getName());
            usernamBox.setText(currentUser.getUsername());
            emailBox.setText(currentUser.getEmail());
            phoneBox.setText(currentUser.getPhoneNumber());
            auth.currentUser = this.currentUser;
        }
    }

    public void logout() {
        firebaseAuth.signOut();
        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void save(View view) {
        String name = nameBox.getText().toString();
        String username = usernamBox.getText().toString();
        String email = emailBox.getText().toString();
        String phoneNumber = phoneBox.getText().toString();

        if (TextUtils.isEmpty(name) && TextUtils.isEmpty(username) && TextUtils.isEmpty(email)) {
            Toast.makeText(EditProfileActivity.this, "Please fill required fields", Toast.LENGTH_SHORT).show();
            return;
        }

        updateData(name, email, username, phoneNumber);
    }

    private void updateData(String name, final String email, String username, String emails) {

        final KProgressHUD kProgressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        Map map = new HashMap();
        map.put("name", name);
        map.put("username", username);
        map.put("email", email);
        map.put("phoneNumber", emails);
        map.put("endSubsDate", FieldValue.serverTimestamp());

        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).update(map).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()) {
                    kProgressHUD.dismiss();
                    updateEmail(email, kProgressHUD);
                } else {
                    kProgressHUD.dismiss();
                    Toast.makeText(EditProfileActivity.this, "Error in data update process", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void updateEmail(String email, final KProgressHUD kProgressHUD) {
        firebaseAuth.getCurrentUser().updateEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    kProgressHUD.dismiss();
                    Toast.makeText(EditProfileActivity.this, "User Data updated", Toast.LENGTH_SHORT).show();
                } else {
                    kProgressHUD.dismiss();
                    Toast.makeText(EditProfileActivity.this, "Could not update email", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}