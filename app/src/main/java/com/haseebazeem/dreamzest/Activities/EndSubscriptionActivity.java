package com.haseebazeem.dreamzest.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.haseebazeem.dreamzest.Helper.Rest;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Viewmodel.CancelViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.kaopiz.kprogresshud.KProgressHUD;

public class EndSubscriptionActivity extends AppCompatActivity implements NetworkResponseListener<BaseResponse<String>> {
    View llEnd;
    private CancelViewModel mCancelViewModel;
    KProgressHUD kProgressHUD;
    String email, userId, subscriptionId, type;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_subscription);
        toolbar = findViewById(R.id.reg_tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Subscription");
        mCancelViewModel = ViewModelProviders.of(this).get(CancelViewModel.class);
        mCancelViewModel.setNavigator(this);
        subscriptionId = getIntent().getStringExtra("subscriptionId");
        type = getIntent().getStringExtra("type");

        kProgressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        llEnd = findViewById(R.id.ll_end);
        llEnd.setOnClickListener(v -> {
            kProgressHUD.show();
            mCancelViewModel.CancelSubscribe(subscriptionId, type);

        });
    }

    @Override
    public void onSuccess(BaseResponse<String> response) {
        kProgressHUD.dismiss();
        Rest.cancelSubscription(new Rest.RequestCallBack() {
            @Override
            public void onComplete() {
                Intent intent = new Intent(EndSubscriptionActivity.this, SubscriptionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
//                finish();


            }

            @Override
            public void onError() {
                Toast.makeText(EndSubscriptionActivity.this, "This user does not exist any more", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onError(BaseResponse<String> response) {
        kProgressHUD.dismiss();
        if (response.getMessage() != null)
            Toast.makeText(EndSubscriptionActivity.this, response.getMessage(), Toast.LENGTH_LONG).show();
        else
            Toast.makeText(EndSubscriptionActivity.this, "Something went wrong.", Toast.LENGTH_LONG).show();
    }


    @Override
    public void jsonParseException(Throwable error) {
        kProgressHUD.dismiss();
        Toast.makeText(EndSubscriptionActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void netWorkException(Throwable error) {
        kProgressHUD.dismiss();
        Toast.makeText(EndSubscriptionActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void unKnownException(Throwable error) {
        kProgressHUD.dismiss();
        Toast.makeText(EndSubscriptionActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();

    }
}