package com.haseebazeem.dreamzest.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Group;
import androidx.lifecycle.ViewModelProviders;

import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Viewmodel.InterpretationViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.keywords.Interpretation;

public class DetailActivity extends AppCompatActivity implements NetworkResponseListener<BaseResponse<Interpretation>> {
    private InterpretationViewModel mInterpretationViewModel;
    Toolbar toolbar;
    private Group gpMain, gpNoInternet;

    private ProgressBar progressBar;
    private Button btnTryAgain;
    private TextView tvKeyword, tvDescription, tvBibleSolution, tvIslamicSolution, tvNoData;
    private int id;
    String subscriptionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        init();
        mInterpretationViewModel = ViewModelProviders.of(this).get(InterpretationViewModel.class);
        mInterpretationViewModel.setNavigator(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnTryAgain.setOnClickListener(v -> {

        });
        id = getIntent().getIntExtra("id", 0);
        subscriptionId = getIntent().getStringExtra("subscriptionId");
        getInterpretation();
    }

    private void getInterpretation() {
        hideAllViews();
        progressBar.setVisibility(View.VISIBLE);
//        mInterpretationViewModel.getInterpretation("sub_1JfjkIDCbYLFSlByr6mC7Qqd", id);
        mInterpretationViewModel.getInterpretation(subscriptionId, id);
    }

    void hideAllViews() {

        gpMain.setVisibility(View.GONE);
        gpNoInternet.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        tvNoData.setVisibility(View.GONE);


    }

    @Override
    public void onSuccess(BaseResponse<Interpretation> response) {
        hideAllViews();
        gpMain.setVisibility(View.VISIBLE);
        if (response.getData() != null) {
            tvKeyword.setText(getIntent().getStringExtra("keyword"));
            tvDescription.setText(response.getData().getInterpretation());
            if (response.getData().getBiblicSolutions() != null)
                tvBibleSolution.setText(response.getData().getBiblicSolutions());
            else
                tvBibleSolution.setText("No Solution Found.");

            if (response.getData().getQuranicSolutions() != null)
                tvIslamicSolution.setText(response.getData().getQuranicSolutions());
            else
                tvIslamicSolution.setText("No Solution Found.");

        }

    }

    @Override
    public void onError(BaseResponse<Interpretation> response) {
        hideAllViews();
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText(response.getMessage());
    }

    @Override
    public void jsonParseException(Throwable error) {
        hideAllViews();
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText(error.getMessage());
    }

    @Override
    public void netWorkException(Throwable error) {

        hideAllViews();
        gpNoInternet.setVisibility(View.VISIBLE);


    }

    @Override
    public void unKnownException(Throwable error) {


        hideAllViews();
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText(error.getMessage());
    }


    private void init() {
        gpMain = findViewById(R.id.gp_main);
        gpNoInternet = findViewById(R.id.gp_no_internet);

        progressBar = findViewById(R.id.progress_bar);

        btnTryAgain = findViewById(R.id.btn_no_internet_try);
        toolbar = findViewById(R.id.reg_tb);
        tvKeyword = findViewById(R.id.tv_keyword);
        tvDescription = findViewById(R.id.tv_description);
        tvBibleSolution = findViewById(R.id.tv_bible_solution);
        tvIslamicSolution = findViewById(R.id.tv_islamic_solution);
        tvNoData = findViewById(R.id.tv_no_data);


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}