package com.haseebazeem.dreamzest.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import com.haseebazeem.dreamzest.R;

public class PrivacyActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy);

        textView = findViewById(R.id.privacy_text);
        toolbar = findViewById(R.id.reg_tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Privacy Policy");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textView.setText(
                        "1. OUR PLEDGE TO YOU\n" +
                        "Our top most priorities are to make sure that the information we have about you is protected and secure. We respect and value our relationship with you and work hard to preserve your privacy and ensure that your preferences are honored. At the same time, the very nature of our relationship may result in us collecting or sharing certain types of information about you. We explain how we use customer information in this privacy policy statement.\n" +
                        "\n" +
                        "2. INFORMATION WE COLLECT\n" +
                        "\n" +
                        "We collect information: You give to us on applications, surveys, registration forms, etc.; you give to us when you make a purchase, or other account information such as balance, payment history or credit/debit card usage; and you give to us when you submit a dream, a question, an experience, or a sleep issue.\n" +
                        "\n" +
                        "3. HOW WE SHARE INFORMATION\n" +
                        "\n" +
                        "We may share information with companies outside our corporate family that perform services on our behalf. We may share information to respond to your inquiries, or provide information about our content and products or services that we offer. We share data provided by customers, purchase/account data, and verifying data. We do not share information about you with non-affiliates (companies outside our organization) for them to contact you for their own marketing purposes. The law permits us to share information about our current and former customers with government agencies or authorized third parties under certain circumstances. For example, we may be required to share such information in response to subpoenas or to comply with certain laws.\n" +
                        "\n" +
                        "4. HOW WE PROTECT INFORMATION\n" +
                        "\n" +
                        "We strive to protect your data and safeguard it from those not authorized to see it."
        );
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}