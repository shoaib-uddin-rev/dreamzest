package com.haseebazeem.dreamzest.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.TextView;

import com.haseebazeem.dreamzest.R;

public class TermsActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        textView = findViewById(R.id.privacy_text);
        toolbar = findViewById(R.id.reg_tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Terms Of Use");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        textView.setText(
                "Please read these Terms of Use carefully before using, submitting any content, products, or services through dreamzest.com.\n" +
                        "\n" +
                        "Your use of this website and related services are provided subject to your compliance with the Terms and Conditions of Use specified below. YOUR CONTINUED USE OF THIS WEBSITE WILL INDICATE YOUR AGREEMENT TO BE BOUND BY THESE TERMS AND CONDITIONS OF USE. IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS OF USE, YOUR ARE REQUESTED TO EXIT THIS SITE.\n" +
                        "\n" +
                        "1. Use of the Website: This website(dreamzest.com) is designed for informational purposes only, and is not intended to be a substitute for medical profession or psychological advice, diagnosis, or treatment. Users of this Website should not rely on information provided in this website for their own health needs. If you have a health related, personal or interpersonal problem, or any specific medical or psychological questions, you are requested to consult with your health consultant.\n" +
                        "\n" +
                        "2. Copyright: You acknowledge and agree that all content and materials available on this Website are protected by copyrights, trademarks, service marks, proprietary rights and laws. You agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit, or create derivative works from such materials or content.\n" +
                        "\n" +
                        "3. Trademark: The following are trademarks and servicemarks owned by or licensed to DreamZest or its affiliates: the name “DreamZest” and “May your good dreams come true”; and any mark used on the website with a “TM”, “SM” or Registered Mark that are not the property of third parties.\n" +
                        "\n" +
                        "4. Links to Other Sites: Links to third party websites on this site are provided solely as a convenience. If you use these links, you will leave this site. DremZest does not claim to have reviewed these third party sites in their entirety, nor does it control these sites. DremZest is not responsible for any of these sites nor their content. If you decide to access any of the third party sites linked to this site, you do so entirely at your own risk.\n" +
                        "\n" +
                        "5. Disclaimer: All information on this site is not a definitive nor an authoritative interpretation on dreams. Information provided on this site is for personal entertainment, reference, and educational purposes. Content on this site is not the final word on dreams and may not be applicable to everyone. “DreamZest” shall not be liable for any direct, indirect, incidental, or consequential damages or harm arising from the use of any content on this site, regardless of the accuracy or completeness of the content. “DreamZest” further does not warrant the accuracy and completeness of the materials at this site. “DreamZest” makes no commitment to update the materials at this site, even if out of date. Dream interpretation is a subjective study. Making important decisions based solely on DreamZest’ interpretation and analysis is unwise and strongly discouraged. Use your own intuition, instincts, and common sense. Use and access to the dreamzest.com site indicates that you understand and agree with all the terms and conditions in the disclaimer.\n" +
                        "\n" +
                        "6. User Submissions: Subject to the “DreamZest” Privacy Statement, any material, information or other communication you transmit or post to this site will be considered non-confidential and non-proprietary. “DreamZest” will have no obligations with respect to such communications. “DreamZest” and its designees will be free to copy, disclose, distribute, incorporate and otherwise use the communications and all data, images, sounds, text, and other things embodied therein for any and all commercial or non-commercial purposes. You are prohibited from posting or transmitting to or from this site any unlawful, threatening, libelous, defamatory, obscene, pornographic, or other material that would violate any law.\n" +
                        "\n" +
                        "7. Revisions: “DreamZest” may revise and update these Terms and Conditions of Use at any time. Please periodically review the terms, conditions, and privacy statements posted on the www.dreamzest.com website. Continued usage of the www.dreamzest.com website will be considered acceptance of any changes. Certain provisions of these terms and conditions may be superseded by expressly designated legal notices or terms on particular pages at this site.\n" +
                        "\n" +
                        "8. Entire Agreement: Except as expressly provided in a particular legal notice on the www.dreamzest.com site, these Terms and Conditions of Use constitute the entire agreement between you and DreamZest with respect to the use of the site and its content."
        );
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}