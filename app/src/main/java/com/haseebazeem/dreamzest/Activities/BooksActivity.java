package com.haseebazeem.dreamzest.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.haseebazeem.dreamzest.Adapters.BooksAdapter;
import com.haseebazeem.dreamzest.MainActivity;
import com.haseebazeem.dreamzest.Model.Book;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class BooksActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Toolbar toolbar;
    List<Book> bookList = new ArrayList<>();
    private FirebaseFirestore firestore;
    private FirebaseAuth firebaseAuth;
    private User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        bookList.add(new Book("BOOK-1", "https://docs.google.com/spreadsheets/d/1of4eCYFOoE-rCxgbjTze2WQJdT1yen887J10AaMZgjA/edit?usp=sharing"));
        bookList.add(new Book("BOOK-2", "https://docs.google.com/spreadsheets/d/1wD957qya5BMrhgPBhQnLv9pT7ks4NAqPoOWsO7NTCoQ/edit?usp=sharing"));
        bookList.add(new Book("BOOK-4", "https://docs.google.com/spreadsheets/d/16xQzoiKK1XZNM8ZXdt6AEXsQLg6oa8lv9kEsmwdoHrA/edit?usp=sharing"));
        bookList.add(new Book("BOOK-6", "https://docs.google.com/spreadsheets/d/1_lhNbAuxg86IGP7o4gAUpnxAjZ0PLz8y3YW0e9jXwow/edit?usp=sharing"));
        bookList.add(new Book("BOOK-11", "https://docs.google.com/spreadsheets/d/1R5R0h-WjxbkY-Xg6akIaJy-VGT_Qil206Mr4n6jqOmg/edit?usp=sharing"));
        bookList.add(new Book("BOOK-22", "https://docs.google.com/spreadsheets/d/1Ka_Q6pz7_2C7swUHEP4pGl2F7oLZiMOnsfgyirCcIiI/edit?usp=sharing"));
        bookList.add(new Book("BOOK-44", "https://docs.google.com/spreadsheets/d/1JcKWh5VnITT_zFGVlbsxJ6ClTw1b8E0-SKIixwqapEE/edit?usp=sharing"));
        bookList.add(new Book("BOOK-66", "https://docs.google.com/spreadsheets/d/1ZVgRrAI_nyxsGauVsovnFsEbNjVMZr592ggpbeEGMzQ/edit?usp=sharing"));

        toolbar = findViewById(R.id.reg_tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        firestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();

        recyclerView = findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        fetchUser();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void fetchUser() {
       final KProgressHUD kProgressHUD = KProgressHUD.create(BooksActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    currentUser = new User(documentSnapshot.getData());
                    BooksAdapter booksAdapter = new BooksAdapter(BooksActivity.this, bookList, currentUser.getEndSubsDate());
                    recyclerView.setAdapter(booksAdapter);
                    kProgressHUD.dismiss();
                } else {
                    kProgressHUD.dismiss();
                    currentUser = null;
                    Toast.makeText(BooksActivity.this, "This user does not exist any more", Toast.LENGTH_SHORT).show();
                    firebaseAuth.signOut();
                    startActivity(new Intent(BooksActivity.this, MainActivity.class));
                    finish();
                }
            }
        });
    }
}