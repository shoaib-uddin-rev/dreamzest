package com.haseebazeem.dreamzest.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.haseebazeem.dreamzest.Helper.Rest;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.haseebazeem.dreamzest.Viewmodel.PaymentViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.PaymentResultWeb;
import com.haseebazeem.dreamzest.data.model.Subscription;
import com.kaopiz.kprogresshud.KProgressHUD;

import static com.haseebazeem.dreamzest.Utils.Constants.RC_WEB_PAYMENT;

public class PaymentActivity extends AppCompatActivity implements NetworkResponseListener<BaseResponse<Subscription>> {


    Button btnPayNow, btnPaypal;
    EditText etCard, etName, etMM, etYY, etCvc;
    Toolbar toolbar;
    PaymentViewModel mPaymentViewModel;
    String email, userId;
    KProgressHUD kProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        mPaymentViewModel = ViewModelProviders.of(this).get(PaymentViewModel.class);
        mPaymentViewModel.setNavigator(this);
        toolbar = findViewById(R.id.reg_tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnPayNow = findViewById(R.id.btn_pay_now);
        btnPaypal = findViewById(R.id.btn_paypal);
        etName = findViewById(R.id.name);
        etCard = findViewById(R.id.card_no);
        etMM = findViewById(R.id.mm);
        etYY = findViewById(R.id.yy);
        etCvc = findViewById(R.id.cvc);
        email = getIntent().getStringExtra("email");
        userId = getIntent().getStringExtra("userId");
        kProgressHUD = KProgressHUD.create(this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);

        btnPayNow.setOnClickListener(v -> {
            if (validate()) {
                showProgress();
                mPaymentViewModel.subscribe(etCard.getText().toString(), etMM.getText().toString(), etYY.getText().toString(), etCvc.getText().toString(), "price_1Jc7UVDCbYLFSlBy17WIewSj", userId, email);
            }
        });
        btnPaypal.setOnClickListener(v -> {
            startActivityForResult(new Intent(this, WebViewActivity.class).putExtra("url", "https://dreamzest.thesupportonline.net/api/paypalSubscriptionPage?firebase_user_id=" + userId + "&email=" + email), RC_WEB_PAYMENT);
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RC_WEB_PAYMENT) {

            if (data != null && data.hasExtra("result")) {
                PaymentResultWeb paymentResultWeb = new Gson().fromJson(data.getStringExtra("result"), PaymentResultWeb.class);
                if (paymentResultWeb.getSuccess() != null && paymentResultWeb.getSuccess() && paymentResultWeb.getData() != null) {
                    showProgress();
                    Rest.updateSubscription("paypal_subscription_id", paymentResultWeb.getData(), new Rest.RequestCallBack() {
                        @Override
                        public void onComplete() {
                            hideProgress();
//                            setResult(RESULT_OK);
//                            finish();

                            Intent intent = new Intent(PaymentActivity.this, DashboardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
//                            finish();

                        }

                        @Override
                        public void onError() {
                            hideProgress();
                            Toast.makeText(PaymentActivity.this, "There is an Error while saving Data", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    if (paymentResultWeb.getMessage() != null)
                        Toast.makeText(this, paymentResultWeb.getMessage(), Toast.LENGTH_LONG
                        ).show();
                    else
                        Toast.makeText(this, "Something went wrong from backend side.", Toast.LENGTH_LONG
                        ).show();
                }
            }
        }
    }

    private boolean validate() {
        if (!Utils.isNotNullEmpty(etCard.getText().toString())) {
            Toast.makeText(this, "Card No can't be blank", Toast.LENGTH_SHORT).show();
            return false;
        }


        if (!Utils.isNotNullEmpty(etName.getText().toString())) {
            Toast.makeText(this, "Name can't be blank", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Utils.isNotNullEmpty(etMM.getText().toString())) {
            Toast.makeText(this, "Expiry Month can't be blank", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Utils.isNotNullEmpty(etYY.getText().toString())) {
            Toast.makeText(this, "Expiry Year No can't be blank", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (!Utils.isNotNullEmpty(etCvc.getText().toString())) {
            Toast.makeText(this, "Cvc No can't be blank", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etYY.getText().toString().length() < 4) {
            Toast.makeText(this, "Invalid  Expiry Year", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etCard.getText().toString().length() < 16) {
            Toast.makeText(this, "Invalid Card No", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onSuccess(BaseResponse<Subscription> response) {
        hideProgress();

        if (response.getData() != null && response.getData().getSubscriptionId() != null) {
            showProgress();
            Rest.updateSubscription("stripe_subscription_id", response.getData().getSubscriptionId(), new Rest.RequestCallBack() {
                @Override
                public void onComplete() {
                    hideProgress();
                    Intent intent = new Intent(PaymentActivity.this, DashboardActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK |
                            Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                @Override
                public void onError() {
                    hideProgress();
                    Toast.makeText(PaymentActivity.this, "There is an Error while saving Data", Toast.LENGTH_LONG).show();
                }
            });

        } else if (response.getMessage() != null) {
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_LONG).show();
        } else
            Toast.makeText(this, "Something went wrong while payment process", Toast.LENGTH_LONG).show();
    }

    void showProgress() {
        kProgressHUD.show();
    }

    void hideProgress() {
        kProgressHUD.dismiss();
    }

    @Override
    public void onError(BaseResponse<Subscription> response) {
        hideProgress();
        if (response.getMessage() != null)
            Toast.makeText(this, response.getMessage(), Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "Something went wrong.", Toast.LENGTH_LONG).show();

//        loading = false;
//        hideAllViews();
//        tvNoData.setVisibility(View.VISIBLE);
//        tvNoData.setText(response.getMessage());
    }

    @Override
    public void jsonParseException(Throwable error) {
        hideProgress();
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();

//        tvNoData.setVisibility(View.VISIBLE);
//        tvNoData.setText(error.getMessage());
    }

    @Override
    public void netWorkException(Throwable error) {
        hideProgress();
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();

    }

    @Override
    public void unKnownException(Throwable error) {
        hideProgress();

        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}