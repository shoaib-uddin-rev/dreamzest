package com.haseebazeem.dreamzest.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.haseebazeem.dreamzest.MainActivity;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Utils;

import javax.annotation.Nullable;

public class SplashActivity extends AppCompatActivity {
    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;
    private User currentUser;
    private ListenerRegistration listenerRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
//                startActivity(intent);
//                finish();
                if (firebaseAuth.getCurrentUser() != null) {
                    listenerRegistration = firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(listener);

                } else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 3000);
    }

    EventListener<DocumentSnapshot> listener = new EventListener<DocumentSnapshot>() {

        @Override
        public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
            if (documentSnapshot.exists()) {
                currentUser = new User(documentSnapshot.getData());
                if (Utils.isSubscriptionNull(currentUser.getPayPalSubscriptionId()) && Utils.isSubscriptionNull(currentUser.getStripeSubscriptionId())) {
                    Intent intent = new Intent(SplashActivity.this, SubscriptionActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);
                    startActivity(intent);
                    finish();
                }

            }
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        if (listenerRegistration != null)
            listenerRegistration.remove();

    }
}