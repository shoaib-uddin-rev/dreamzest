package com.haseebazeem.dreamzest.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;


public class SubscriptionActivity extends AppCompatActivity {
    CardView subscribeCard;
    String email, userId, subscriptionId, type;
    Toolbar toolbar;
    private FirebaseAuth firebaseAuth;
    public User currentUser;
    private FirebaseFirestore firestore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        toolbar = findViewById(R.id.reg_tb);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Subscription");
        subscribeCard = findViewById(R.id.subscribe_card);
        subscribeCard.setOnClickListener(v -> {
            startActivity(new Intent(this, PaymentActivity.class).putExtra("email", email).putExtra("userId", userId));
        });

        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    currentUser = new User(documentSnapshot.getData());
                    email = currentUser.getEmail();
                    userId = currentUser.getUserID();
                    Log.d("CURRENT_USER", "onSuccess: " + currentUser);
                } else {
                    currentUser = null;
                    Toast.makeText(getBaseContext(), "This user does not exist any more", Toast.LENGTH_SHORT).show();
//                    logout();
                }
            }
        });

    }
}