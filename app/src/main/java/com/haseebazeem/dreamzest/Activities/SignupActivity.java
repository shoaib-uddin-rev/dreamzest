package com.haseebazeem.dreamzest.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.model.value.ServerTimestampValue;
import com.google.firestore.v1beta1.DocumentTransform;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.kaopiz.kprogresshud.KProgressHUD;

public class SignupActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;
    Toolbar tb;
    EditText name, username, email, pass, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();

        tb = findViewById(R.id.reg_tb);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        name = findViewById(R.id.name);
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.password);
        phone = findViewById(R.id.phone);
    }

    public void signup(View view) {
        final String emailText = email.getText().toString();
        final String passText = pass.getText().toString();
        String nameText = name.getText().toString();
        final String usernameText = username.getText().toString();

        final KProgressHUD kProgressHUD = KProgressHUD.create(SignupActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        if(TextUtils.isEmpty(nameText) || TextUtils.isEmpty(usernameText) || TextUtils.isEmpty(emailText) || TextUtils.isEmpty(passText)) {
            Toast.makeText(this, "Please fill all the required fields", Toast.LENGTH_SHORT).show();
            return;
        }

        CollectionReference usersRef = firestore.collection("users");
        Query query = usersRef.whereEqualTo("username", usernameText);
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for(DocumentSnapshot documentSnapshot : task.getResult()){
                        String user = documentSnapshot.getString("username");

                        if(user.equals(usernameText)){
                            kProgressHUD.dismiss();
                            Toast.makeText(SignupActivity.this, "This Username already exists. Try Another", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                if(task.getResult().size() == 0 ){
                    authUser(emailText, passText, kProgressHUD);
                }
            }
        });

    }

    private void authUser(final String email, String pass, final KProgressHUD kProgressHUD) {
        final String phoneNumber = phone.getText().toString();

        firebaseAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    String nameText = name.getText().toString();
                    String usernameText = username.getText().toString();
                    String profile_img = "https://firebasestorage.googleapis.com/v0/b/cdm-manager.appspot.com/o/default_android.png?alt=media&token=6b2ba0ab-2230-49ee-94fb-31598babe75f";
                    User user = new User();
                    user.setName(nameText);
                    user.setUsername(usernameText);
                    user.setEmail(email);
                    user.setAvatar(profile_img);
                    user.setUserID(firebaseAuth.getCurrentUser().getUid());
                    user.setEndSubsDate(Utils.getCurrentDate());
                    user.setPhoneNumber(phoneNumber);

                    saveToDB(user, kProgressHUD);

                } else {
                    kProgressHUD.dismiss();
                    handleExceptions(task);
                    Toast.makeText(SignupActivity.this, "Could not register. Try Again", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void saveToDB(User user, final KProgressHUD kProgressHUD) {

        firestore.collection("users").document(user.getUserID()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(SignupActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(SignupActivity.this, DashboardActivity.class));
                    finish();
                } else {
                    kProgressHUD.dismiss();
                    Toast.makeText(SignupActivity.this, "Error in saving user", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void handleExceptions(@NonNull Task<AuthResult> task) {
        try
        {
            throw task.getException();
        }
        // if user enters wrong email.
        catch (FirebaseAuthWeakPasswordException weakPassword)
        {
            Toast.makeText(this, "Please Enter strong password", Toast.LENGTH_SHORT).show();
        }
        // if user enters wrong password.
        catch (FirebaseAuthInvalidCredentialsException malformedEmail)
        {
            Toast.makeText(this, "Please give valid email", Toast.LENGTH_SHORT).show();
        }
        catch (FirebaseAuthUserCollisionException existEmail)
        {
            Toast.makeText(this, "This email already registered. Use other one", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e)
        {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}