package com.haseebazeem.dreamzest.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.haseebazeem.dreamzest.MainActivity;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Auth;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.kaopiz.kprogresshud.KProgressHUD;

import javax.annotation.Nullable;

public class LoginActivity extends AppCompatActivity {

    Toolbar tb;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;
    EditText email, pass;
    private Auth auth;

    KProgressHUD kProgressHUD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        tb = findViewById(R.id.reg_tb);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.password);
        setSupportActionBar(tb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void goToDashboard(View view) {
        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

    public void login(View view) {

        kProgressHUD = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        String emailText = email.getText().toString();
        String passText = pass.getText().toString();

        if(TextUtils.isEmpty(emailText) && TextUtils.isEmpty(passText)) {
            kProgressHUD.dismiss();
            return;
        }

        firebaseAuth.signInWithEmailAndPassword(emailText, passText).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    auth = new Auth(LoginActivity.this);
                    kProgressHUD.dismiss();
                    kProgressHUD.show();
                    firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
                        @Override
                        public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                            kProgressHUD.dismiss();
                            User user = new User(documentSnapshot.getData());
                            if (Utils.isSubscriptionNull(user.getPayPalSubscriptionId()) && Utils.isSubscriptionNull(user.getStripeSubscriptionId())) {
                                Intent intent = new Intent(LoginActivity.this, SubscriptionActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    });



//                    Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
//                    startActivity(intent);
//                    finish();
                } else {
                    kProgressHUD.dismiss();
                    handleException(task);
                }
            }
        });

    }

    private void handleException(@NonNull Task<AuthResult> task){
        try
        {
            throw task.getException();
        }
        // if user enters wrong email.
        catch (FirebaseAuthInvalidUserException invalidEmail)
        {
//            Log.d(TAG, "onComplete: invalid_email");
            Toast.makeText(this, "There is no account with this email", Toast.LENGTH_SHORT).show();

            // TODO: take your actions!
        }
        // if user enters wrong password.
        catch (FirebaseAuthInvalidCredentialsException wrongPassword)
        {
//            Log.d(TAG, "onComplete: wrong_password");
            Toast.makeText(this, "You have entered wrong password", Toast.LENGTH_SHORT).show();

            // TODO: Take your action
        }
        catch (Exception e)
        {
            Toast.makeText(this, "Wrong Email or Password", Toast.LENGTH_SHORT).show();
//            Log.d(TAG, "onComplete: " + e.getMessage());
        }
    }

    public void goReset(View view) {
        startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
    }
}