package com.haseebazeem.dreamzest.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.Group;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.haseebazeem.dreamzest.Adapters.KeywordsAdapter;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.LogUtil;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.haseebazeem.dreamzest.Viewmodel.KeywordsViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.keywords.Keyword;
import com.haseebazeem.dreamzest.data.model.keywords.KeywordList;

import java.util.List;

import io.reactivex.functions.Consumer;

public class KeywordsActivity extends AppCompatActivity implements NetworkResponseListener<BaseResponse<KeywordList>> {
    private static final String TAG = "KeyWordsActivity";
    Toolbar toolbar;
    private KeywordsViewModel mKeywordsViewModel;
    int page = 1;
    BaseResponse<KeywordList> mKeyWordsResponse;
    private KeywordsAdapter adapter;
    private List<Keyword> keywords;
    private int totalPages;
    private boolean loading = false;
    private Group gpMain, gpNoInternet;
    private EditText etSearch;
    private RecyclerView rvKeywords;
    private ProgressBar progressBar, secondaryProgress;
    private Button btnTryAgain, btnReset;
    private TextView tvNoData;
    private NestedScrollView nestedScrollView;
    String subscriptionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keywords);


        mKeywordsViewModel = ViewModelProviders.of(this).get(KeywordsViewModel.class);
        mKeywordsViewModel.setNavigator(this);
        subscriptionId = getIntent().getStringExtra("subscriptionId");
        init();
        setBtnListener();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

//        mKeywordsViewModel.searchByQuery(etSearch, "sub_1JfjkIDCbYLFSlByr6mC7Qqd", page, str -> {
        mKeywordsViewModel.searchByQuery(etSearch, subscriptionId, page, str -> {

            page = 1;
        }, searchResponse, throwable -> {
            LogUtil.e(TAG, "throwable " + throwable);

        });
        getKeywords();

    }

    Consumer<BaseResponse<KeywordList>> searchResponse = response -> {
        loading = false;
        if (response.getSuccess()) {
            if (response.getData() != null && response.getData().getData().size() > 0) {
                mKeyWordsResponse = response;
                if (page <= 1) {
                    keywords = null;
                    adapter.notifyDataSetChanged();

                    keywords = mKeyWordsResponse.getData().getData();
                    adapter = new KeywordsAdapter(this, keywords, subscriptionId);
                    rvKeywords.setAdapter(adapter);
                } else {

                    int oldItems = keywords.size();
                    keywords.addAll(mKeyWordsResponse.getData().getData());
                    adapter.notifyItemRangeInserted(oldItems, oldItems + mKeyWordsResponse.getData().getData().size());
                }
                ++page;

            } else {
                hideAllViews();
                tvNoData.setVisibility(View.VISIBLE);
                btnReset.setVisibility(View.VISIBLE);

            }

        }
    };

    private void setBtnListener() {
        btnTryAgain.setOnClickListener(v -> {
            getKeywords();
        });
        btnReset.setOnClickListener(v -> {
            Utils.hideKeyboard(this);
            etSearch.setText("");
            page = 1;
            keywords = null;
            getKeywords();
        });
    }

    private void init() {
        gpMain = findViewById(R.id.gp_main);
        gpNoInternet = findViewById(R.id.gp_no_internet);
        etSearch = findViewById(R.id.et_search);
        rvKeywords = findViewById(R.id.recycler);
        progressBar = findViewById(R.id.progress_bar);
        secondaryProgress = findViewById(R.id.secondary_progress);
        btnTryAgain = findViewById(R.id.btn_no_internet_try);
        btnReset = findViewById(R.id.btn_reset);
        tvNoData = findViewById(R.id.tv_no_data);
        nestedScrollView = findViewById(R.id.nestedScrollView);
        toolbar = findViewById(R.id.reg_tb);
        rvKeywords.setNestedScrollingEnabled(false);
        nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
            LogUtil.e(TAG, "setOnScrollChangeListener ");
            if (scrollY >= (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()) && scrollY > oldScrollY) {
                LogUtil.e(TAG, "setOnScrollChangeListener going to bottom");
                if (!loading && !isLastPage()) {

                    v.post(() -> {
                        secondaryProgress.setVisibility(View.VISIBLE);
                    });
                    if (Utils.isNotNullEmpty(etSearch.getText().toString())) {
                        searchKeyWords();
                    } else
                        getKeywords();

                    loading = true;

                }
                LogUtil.e(TAG, "laoding " + loading + " isLastPage " + isLastPage());
            }
        });
    }

    private boolean isLastPage() {
        return totalPages < 1 || page > totalPages;
    }

    void getKeywords() {
        if (keywords == null) {
            hideAllViews();
            progressBar.setVisibility(View.VISIBLE);
        }


//        mKeywordsViewModel.getKeywords("sub_1JfjkIDCbYLFSlByr6mC7Qqd", page);
        mKeywordsViewModel.getKeywords(subscriptionId, page);
    }

    void searchKeyWords() {
//        mKeywordsViewModel.getSearches("sub_1JfjkIDCbYLFSlByr6mC7Qqd", etSearch.getText().toString(), page)
        mKeywordsViewModel.getSearches(subscriptionId, etSearch.getText().toString(), page)
                .subscribe(searchResponse, throwable -> {
                    LogUtil.e(TAG, "throwable " + throwable);

                })
        ;
    }

    void updateView() {
        hideAllViews();
        loading = false;
        gpMain.setVisibility(View.VISIBLE);
        totalPages = mKeyWordsResponse.getData().getLastPage();
        LogUtil.e(TAG, "totalPages " + totalPages);
        if (mKeyWordsResponse.getData() != null && mKeyWordsResponse.getData().getData().size() > 0) {

            if (keywords == null) {
                keywords = mKeyWordsResponse.getData().getData();
                adapter = new KeywordsAdapter(this, keywords, subscriptionId);
                rvKeywords.setAdapter(adapter);
//                nestedScrollView.post(() -> nestedScrollView.smoothScrollTo(0, ivBanner.getBottom()));


            } else {
                int oldItems = keywords.size();
                keywords.addAll(mKeyWordsResponse.getData().getData());
                adapter.notifyItemRangeInserted(oldItems, oldItems + mKeyWordsResponse.getData().getData().size());
            }

        } else if (keywords == null) {
            keywords = null;
            if (adapter != null)
                adapter.notifyDataSetChanged();
            hideAllViews();

            tvNoData.setVisibility(View.VISIBLE);

//            if (Util.isNotNullEmpty(appliedFilters) || Util.isNotNullEmpty(sortByOptions))
//                resetButton.setVisibility(View.VISIBLE);

        }

    }

    void hideAllViews() {
        secondaryProgress.setVisibility(View.GONE);
        gpMain.setVisibility(View.GONE);
        gpNoInternet.setVisibility(View.GONE);
        tvNoData.setVisibility(View.GONE);
        btnReset.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);


    }

    @Override
    public void onSuccess(BaseResponse<KeywordList> response) {
        if (response.getData() != null) {


            ++page;
            mKeyWordsResponse = response;
            updateView();
        } else {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onError(BaseResponse<KeywordList> response) {
        loading = false;
        hideAllViews();
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText(response.getMessage());
    }
//
//    @Override
//    public void onResponse(BaseResponse<KeywordList> response) {
//
//    }
//
//    @Override
//    public void onFailure(Throwable error) {
//
//    }
//
//    @Override
//    public void onTokenExpired(BaseResponse<KeywordList> error) {
//
//    }

    @Override
    public void jsonParseException(Throwable error) {
        loading = false;
        hideAllViews();
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText(error.getMessage());
    }

    @Override
    public void netWorkException(Throwable error) {
        loading = false;
        hideAllViews();
        gpNoInternet.setVisibility(View.VISIBLE);


    }

    @Override
    public void unKnownException(Throwable error) {

        loading = false;
        hideAllViews();
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText(error.getMessage());
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}