package com.haseebazeem.dreamzest.Viewmodel;

import android.widget.EditText;

import com.haseebazeem.dreamzest.Utils.RxSearchObservable;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.haseebazeem.dreamzest.base.BaseViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.keywords.Interpretation;
import com.haseebazeem.dreamzest.data.model.keywords.KeywordList;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class InterpretationViewModel extends BaseViewModel<NetworkResponseListener<BaseResponse<Interpretation>>> {

    public InterpretationViewModel() {

    }
public  void  getInterpretation(String subscriptionId, int id){
        getCompositeDisposable().add(makeRequest(getApi().getInterpretation(id,subscriptionId),getNavigator()));
}
}
