package com.haseebazeem.dreamzest.Viewmodel;

import com.haseebazeem.dreamzest.base.BaseViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.Subscription;

public class CancelViewModel extends BaseViewModel<NetworkResponseListener<BaseResponse<String>>> {

    public CancelViewModel() {

    }

    public void CancelSubscribe(String subscriptionId, String type) {
        getCompositeDisposable().add(makeRequest(getApi().cancelSubscription(subscriptionId,type), getNavigator()));
    }
}
