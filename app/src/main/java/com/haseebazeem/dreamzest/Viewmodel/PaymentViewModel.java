package com.haseebazeem.dreamzest.Viewmodel;

import com.haseebazeem.dreamzest.base.BaseViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.Subscription;

public class PaymentViewModel extends BaseViewModel<NetworkResponseListener<BaseResponse<Subscription>>> {

    public PaymentViewModel() {

    }

    public void subscribe(String cardNo,String month,String year, String cvv, String planId, String userId,String email) {
        getCompositeDisposable().add(makeRequest(getApi().subscribe(cardNo, month, year, cvv, planId, userId, email), getNavigator()));
    }
}
