package com.haseebazeem.dreamzest.Viewmodel;

import android.widget.EditText;

import com.haseebazeem.dreamzest.Utils.RxSearchObservable;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.haseebazeem.dreamzest.base.BaseViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.haseebazeem.dreamzest.data.model.keywords.KeywordList;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class KeywordsViewModel extends BaseViewModel<NetworkResponseListener<BaseResponse<KeywordList>>> {

    public KeywordsViewModel() {

    }

    public void getKeywords(String subscriptionId, int page) {

        getCompositeDisposable().add(makeRequest(getApi().getKeywords(subscriptionId, page), getNavigator()));
    }

    public void searchByQuery(EditText etSearch, String subscriptionId, int page, Consumer<String> onNext, Consumer<BaseResponse<KeywordList>> sucess, Consumer<Throwable> err) {
        RxSearchObservable.fromView(etSearch)
                .debounce(50, TimeUnit.MILLISECONDS)
                .filter(Utils::isNotNullEmpty)
                .doOnNext(onNext)
                .subscribeOn(Schedulers.io())
                .distinctUntilChanged()
                .switchMap((Function<String, Observable<BaseResponse<KeywordList>>>) s -> getSearches(subscriptionId, s, page))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sucess, err
//                        response -> {
//                            productsItemList = null;
//                            searchAdapter.notifyDataSetChanged();
//
//                            if (Util.isSuccessResponse(response) && response.getData() != null
//                                    && response.getData().getSearchResults() != null && response.getData().getSearchResults().getProducts() != null)
//                                productsItemList = response.getData().getSearchResults().getProducts();
//                            searchAdapter.notifyDataSetChanged();
//                        }, throwable -> {
//                            productsItemList = null;
//                            searchAdapter.notifyDataSetChanged();
//                            LogUtil.e(TAG, "throwable " + throwable);
//                        }
                );
    }

    public Observable<BaseResponse<KeywordList>> getSearches(String subscriptionId, String query, int page) {
        return getApi()
                .searchKeywords(subscriptionId, query, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        //        searchViewModel.getSearchResults(apiToken, s, );

    }
}
