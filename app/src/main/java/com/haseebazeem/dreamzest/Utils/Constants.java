package com.haseebazeem.dreamzest.Utils;

public class Constants {
    public static final String BASE_URL = "https://dreamzest.thesupportonline.net/api/";
    public static final String API_KEYWORD = "keyword";
    public static final String API_KEYWORD_SEARCH = "keywordSearch";
    public static final String API_INTERPRETATION = "interpretation/{id}";
    public static final String API_SUBSCRIBE = "subscribe";
    public static final String API_CANCEL_SUBSCRIPTION = "cancel-subscription";
    public static final int RC_WEB_PAYMENT = 1523;

}
