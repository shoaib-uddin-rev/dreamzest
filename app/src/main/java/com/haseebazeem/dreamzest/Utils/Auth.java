package com.haseebazeem.dreamzest.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.haseebazeem.dreamzest.MainActivity;
import com.haseebazeem.dreamzest.Model.User;

public class Auth {

    public User currentUser;
    private Context context;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;

    public Auth(final Context context) {
        this.context = context;
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        if(isLoggedIn() && this.currentUser == null) {
            fetchUser();
        }
    }

    public void logout() {
        firebaseAuth.signOut();
        Intent intent = new Intent(context, MainActivity.class);
        ((Activity) context).startActivity(intent);
    }

    private void fetchUser() {
        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    currentUser = new User(documentSnapshot.getData());
                    Log.d("CURRENT_USER", "onSuccess: " + currentUser);
                } else {
                    currentUser = null;
                    Toast.makeText(context, "This user does not exist any more", Toast.LENGTH_SHORT).show();
                    logout();
                }
            }
        });
    }

    public User getCurrentUser() {
        return this.currentUser;
    }

    private boolean isLoggedIn() {
        if(firebaseAuth.getCurrentUser() != null) {
            return true;
        }
        return false;
    }

}
