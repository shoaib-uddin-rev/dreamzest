package com.haseebazeem.dreamzest.Utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class RxSearchObservable {
    private static final String TAG = RxSearchObservable.class.getSimpleName();

    private RxSearchObservable() {
        // no instance
    }

    public static Observable<String> fromView(EditText editText) {

        final PublishSubject<String> subject = PublishSubject.create();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                LogUtil.e(TAG, "afterTextChanged " + editable);
                subject.onNext(editable.toString());
            }
        });
//        editText.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String s) {
//                LogUtil.e(TAG, "onQueryTextSubmit " + s);
//                subject.onNext(s);
//                return true;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String text) {
//
//                LogUtil.e(TAG, "onQueryTextChange " + text);
//                subject.onNext(text);
//                return true;
//            }
//        });

        return subject;
    }

}
