package com.haseebazeem.dreamzest.Utils;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class Utils {

    public static String getCurrentDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.UK);
        String date = simpleDateFormat.format(new Date());
        return date;
    }
    /**
     * @param string value to check
     * @return true if not null or empty param string
     */
    public static boolean isNotNullEmpty(@Nullable String string) {
        return !(string == null || string.trim().length() == 0);
    }
    public  static  boolean isSubscriptionNull(@Nullable  String id){
        return (id== null || id.trim().length()==0 || Objects.equals(id, "null"));
    }
    public static void hideKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }
    public static String getRandomString(final int sizeOfRandomString) {
        String ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnm";
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder(sizeOfRandomString);
        for (int i = 0; i < sizeOfRandomString; ++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    public static long daysLeft(String dateStr) {
        long result = 0;
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Date past = format.parse(dateStr);
            Date now = new Date();
            long seconds= TimeUnit.MILLISECONDS.toSeconds(past.getTime() - now.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(past.getTime() - now.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(past.getTime() - now.getTime());
            long days=TimeUnit.MILLISECONDS.toDays(past.getTime() - now.getTime());
            result = days;
            Log.d("DATA", "daysLeft: " + result);
        }
        catch (Exception j){
            j.printStackTrace();
        }

        return result;
    }

    public static String addThirtyDays() {
        Date date = new Date();
        SimpleDateFormat df  = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        String currentDate = df.format(date);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        java.util.Calendar c = java.util.Calendar.getInstance();
        try{
            //Setting the date to the given date
            c.setTime(sdf.parse(currentDate));
        }catch(ParseException e){
            e.printStackTrace();
        }

        //Number of Days to add
        c.add(Calendar.DAY_OF_MONTH, 30);
        //Date after adding the days to the given date
        String newDate = sdf.format(c.getTime());
        return newDate;
    }
}
