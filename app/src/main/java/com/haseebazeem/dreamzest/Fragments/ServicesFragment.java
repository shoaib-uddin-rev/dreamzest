package com.haseebazeem.dreamzest.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.haseebazeem.dreamzest.Activities.DashboardActivity;
import com.haseebazeem.dreamzest.Activities.KeywordsActivity;
import com.haseebazeem.dreamzest.Helper.Rest;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Auth;
import com.haseebazeem.dreamzest.Utils.LogUtil;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.haseebazeem.dreamzest.Viewmodel.CancelViewModel;
import com.haseebazeem.dreamzest.data.NetworkResponseListener;
import com.haseebazeem.dreamzest.data.model.BaseResponse;
import com.kaopiz.kprogresshud.KProgressHUD;

import javax.annotation.Nullable;

import static android.app.Activity.RESULT_OK;
import static com.haseebazeem.dreamzest.Utils.Constants.RC_WEB_PAYMENT;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ServicesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ServicesFragment extends Fragment implements NetworkResponseListener<BaseResponse<String>> {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private TextView nameBox, usernamBox, emailBox, phoneBox;

    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore firestore;
    private Auth auth;
    private User currentUser;
    CardView dreamCard;
    String email, userId, subscriptionId, type;
    private String TAG = "ServiceFragment";
    private Button btnDream;
    private CancelViewModel mCancelViewModel;
    KProgressHUD kProgressHUD;

    public ServicesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ServicesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ServicesFragment newInstance(String param1, String param2) {
        ServicesFragment fragment = new ServicesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mCancelViewModel = ViewModelProviders.of(this).get(CancelViewModel.class);
        mCancelViewModel.setNavigator(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_services, container, false);

        Log.d("DATA", "onCreateView: THIS IS SERVICE FRAGMENT");
        kProgressHUD = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f);
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
//        subscribeCard = view.findViewById(R.id.subscribe_card);
        dreamCard = view.findViewById(R.id.find_dream_card);
        auth = new Auth(getContext());
        nameBox = view.findViewById(R.id.name);
        usernamBox = view.findViewById(R.id.username);
        emailBox = view.findViewById(R.id.email);
        phoneBox = view.findViewById(R.id.phone);
//        btnCancel = view.findViewById(R.id.btn_cancel);
        btnDream = view.findViewById(R.id.btn_dream);

//        subscribeCard.setOnClickListener(v -> {

//            startActivityForResult(new Intent(getActivity(), PaymentActivity.class).putExtra("email", email).putExtra("userId", userId), RC_WEB_PAYMENT);
//        });

        dreamCard.setOnClickListener(v -> {

            startActivity(new Intent(getActivity(), KeywordsActivity.class).putExtra("email", email).putExtra("subscriptionId", subscriptionId));
        });
        btnDream.setOnClickListener(v -> {

            startActivity(new Intent(getActivity(), KeywordsActivity.class).putExtra("email", email).putExtra("subscriptionId", subscriptionId));
        });
//        btnCancel.setOnClickListener(v -> {
//            kProgressHUD.show();
//            mCancelViewModel.CancelSubscribe(subscriptionId, type);
//
//        });
//        materialCardView = view.findViewById(R.id.service_card);
//        materialCardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d("DATA", "onClick: " + Utils.addThirtyDays());
////                startActivity(new Intent(getContext(), BooksActivity.class));
//                startActivity(new Intent(getContext(), KeywordsActivity.class));
////
//            }
//        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == RC_WEB_PAYMENT) {
            setUser();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUser();
    }

    private void setUser() {
        if (auth.getCurrentUser() != null) {
            LogUtil.e(TAG, auth.getCurrentUser().toString());
            fillBoxes();
        } else {
            fetchUser();
        }
    }

    private void fetchUser() {
        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    currentUser = new User(documentSnapshot.getData());

                    fillBoxes();
                } else {
                    currentUser = null;
                    Toast.makeText(getActivity(), "This user does not exist any more", Toast.LENGTH_SHORT).show();
//                    logout();
                }
            }
        });
    }

    private void fillBoxes() {
        if (auth.getCurrentUser() != null) {
            nameBox.setText(auth.getCurrentUser().getName());
            usernamBox.setText(auth.getCurrentUser().getUsername());
            emailBox.setText(auth.getCurrentUser().getEmail());
            phoneBox.setText(auth.getCurrentUser().getPhoneNumber());
            email = auth.getCurrentUser().getEmail();
            userId = auth.getCurrentUser().getUserID();
            if (Utils.isSubscriptionNull(auth.getCurrentUser().getPayPalSubscriptionId()) && Utils.isSubscriptionNull(auth.getCurrentUser().getStripeSubscriptionId())) {
//                subscribeCard.setVisibility(View.VISIBLE);
                dreamCard.setVisibility(View.GONE);
//                btnCancel.setVisibility(View.GONE);

            } else {
//                subscribeCard.setVisibility(View.GONE);
                dreamCard.setVisibility(View.VISIBLE);
//                btnCancel.setVisibility(View.VISIBLE);
                if (Utils.isSubscriptionNull(auth.getCurrentUser().getStripeSubscriptionId())) {

                    subscriptionId = auth.getCurrentUser().getPayPalSubscriptionId();
                    type = "paypal";
                } else {

                    subscriptionId = auth.getCurrentUser().getStripeSubscriptionId();
                    type = "stripe";
                }

            }
        } else if (currentUser != null) {
            nameBox.setText(currentUser.getName());
            usernamBox.setText(currentUser.getUsername());
            emailBox.setText(currentUser.getEmail());
            email = currentUser.getEmail();
            userId = currentUser.getUserID();
            phoneBox.setText(currentUser.getPhoneNumber());
            auth.currentUser = this.currentUser;
            if (Utils.isSubscriptionNull(auth.getCurrentUser().getPayPalSubscriptionId()) && Utils.isSubscriptionNull(auth.getCurrentUser().getStripeSubscriptionId())) {
//                subscribeCard.setVisibility(View.VISIBLE);
                dreamCard.setVisibility(View.GONE);
//                btnCancel.setVisibility(View.GONE);

            } else {
//                subscribeCard.setVisibility(View.GONE);
                dreamCard.setVisibility(View.VISIBLE);
//                btnCancel.setVisibility(View.VISIBLE);
                if (Utils.isSubscriptionNull(auth.getCurrentUser().getStripeSubscriptionId())) {

                    subscriptionId = auth.getCurrentUser().getPayPalSubscriptionId();

                    type = "paypal";
                } else {

                    subscriptionId = auth.getCurrentUser().getStripeSubscriptionId();
                    type = "stripe";
                }


            }
        }
    }

    @Override
    public void onSuccess(BaseResponse<String> response) {
        kProgressHUD.dismiss();
//        Rest.cancelSubscription(new Rest.RequestCallBack() {
//            @Override
//            public void onComplete() {
//                Intent intent = new Intent(getActivity(), DashboardActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                        Intent.FLAG_ACTIVITY_CLEAR_TASK |
//                        Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
//
//
//            }
//
//            @Override
//            public void onError() {
//                Toast.makeText(getActivity(), "This user does not exist any more", Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    @Override
    public void onError(BaseResponse<String> response) {
        kProgressHUD.dismiss();
        if (response.getMessage() != null)
            Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getActivity(), "Something went wrong.", Toast.LENGTH_LONG).show();
    }


    @Override
    public void jsonParseException(Throwable error) {
        kProgressHUD.dismiss();
        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void netWorkException(Throwable error) {
        kProgressHUD.dismiss();
        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void unKnownException(Throwable error) {
        kProgressHUD.dismiss();
        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();

    }
}