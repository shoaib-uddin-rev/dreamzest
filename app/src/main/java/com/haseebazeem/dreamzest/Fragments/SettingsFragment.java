package com.haseebazeem.dreamzest.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.haseebazeem.dreamzest.Activities.CheckoutActivity;
import com.haseebazeem.dreamzest.Activities.EditProfileActivity;
import com.haseebazeem.dreamzest.Activities.EndSubscriptionActivity;
import com.haseebazeem.dreamzest.Activities.PrivacyActivity;
import com.haseebazeem.dreamzest.Activities.TermsActivity;
import com.haseebazeem.dreamzest.MainActivity;
import com.haseebazeem.dreamzest.Model.User;
import com.haseebazeem.dreamzest.R;
import com.haseebazeem.dreamzest.Utils.Utils;
import com.kaopiz.kprogresshud.KProgressHUD;

import javax.annotation.Nullable;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SettingsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {

    private FirebaseAuth firebaseAuth;
    private LinearLayout renewBtn, subs;
    private FirebaseFirestore firestore;
    private User currentUser;

    String email, userId, subscriptionId, type;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingsFragment newInstance(String param1, String param2) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        firebaseAuth = FirebaseAuth.getInstance();
        firestore = FirebaseFirestore.getInstance();
        fetchUser();

        LinearLayout editBtn = view.findViewById(R.id.edit_btn);
        LinearLayout signOutBtn = view.findViewById(R.id.signout);
        LinearLayout termsBtn = view.findViewById(R.id.terms);
        LinearLayout privacyBtn = view.findViewById(R.id.privacy);
        subs = view.findViewById(R.id.subs);
        subs.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), EndSubscriptionActivity.class).putExtra("email", email).putExtra("userId", userId).putExtra("subscriptionId", subscriptionId).putExtra("type", type));
        });
        renewBtn = view.findViewById(R.id.renew);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditProfileActivity.class));
            }
        });
        termsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), TermsActivity.class));
            }
        });
        privacyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), PrivacyActivity.class));
            }
        });
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        renewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long days = Utils.daysLeft(currentUser.getEndSubsDate());
                if (days > 0) {
                    openAlert(days);
                } else {
                    startActivity(new Intent(getContext(), CheckoutActivity.class));
                }
            }
        });
        return view;
    }

    public void logout() {
        firebaseAuth.signOut();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        ((Activity) getContext()).finish();
    }

    private void openAlert(long days) {
        new AlertDialog.Builder(getContext())
                .setTitle("Subscription Status")
                .setMessage("You already have " + days + " days subscription left")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    private void fetchUser() {
        final KProgressHUD kProgressHUD = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        firestore.collection("users").document(firebaseAuth.getCurrentUser().getUid()).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    currentUser = new User(documentSnapshot.getData());
                    if (Utils.isSubscriptionNull(currentUser.getStripeSubscriptionId())) {

                        subscriptionId = currentUser.getPayPalSubscriptionId();
                        email = currentUser.getEmail();
                        userId = currentUser.getUserID();

                        type = "paypal";
                    } else {

                        subscriptionId = currentUser.getStripeSubscriptionId();
                        type = "stripe";
                    }
                    kProgressHUD.dismiss();
                } else {
                    kProgressHUD.dismiss();
                    currentUser = null;
                    Toast.makeText(getContext(), "This user does not exist any more", Toast.LENGTH_SHORT).show();
                    logout();
                }
            }
        });
    }

}